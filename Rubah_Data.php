<?php
include 'Koneksi.php';
include 'Prodi.php';
include 'Gander.php';
if (count($prodi) == 0) {
    echo '<p>Data prodi belum tersedia</p>';
    echo '<a href="Halaman_Utama.php"><button type="submit" class="btn btn-warning">Halaman Utama</button></a>';
    mysqli_close($link);
    exit();
}

$nim = '';
if (isset($_GET['nim'])) $nim = $_GET['nim'];
$mahasiswa = [];
$result = mysqli_query($link, "SELECT nim,nama_mahasiswa,prodi_id,gander_id FROM
mahasiswa WHERE nim='$nim'");
if ($result) {
    if ($row = mysqli_fetch_row($result)) {
        $mahasiswa['nim'] = $row[0];
        $mahasiswa['nama_mahasiswa'] = $row[1];
        $mahasiswa['prodi_id'] = $row[2];
        $mahasiswa['gander_id'] = $row[3];
    }
    mysqli_free_result($result);
}
if (count($mahasiswa) == 0) {
    echo '<p>Data mahasiswa tidak ada</p>';
    echo '<a href="Halaman_Utama.php"><button type="submit" class="btn btn-warning">Halaman Utama</button></a>';
    mysqli_close($link);
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rubah Data</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        .mx-auto {
            width: 1000px;
        }

        .card {
            margin-top: 10px;
        }
    </style>
</head>

<body>
    <div class="mx-auto">
        <div class="card">
            <div class="card-header text-white bg-secondary">
                EDIT DATA
            </div>
            <div class="card-body">
                <form action="Update.php" method="post">
                    <div class="mb-3 row">
                        <label for="nim">NIM</label>
                        <input type="text" name="nim" value="<?php echo $mahasiswa['nim']; ?>" readonly="readonly" />
                        <label for="nama_mahasiswa">Nama</label>
                        <input type="text" name="nama_mahasiswa" value="<?php echo $mahasiswa['nama_mahasiswa']; ?>" />
                        <label for="prodi_id">Prodi</label>
                        <select name="prodi_id">
                            <?php
                            foreach ($prodi as $prodi) {
                                $selected = '';
                                if ($prodi['id'] == $mahasiswa['prodi_id']) $selected = 'selected';
                                echo '<option value="' . $prodi['id'] . '" ' . $selected . '>' . $prodi['prodi'] . '</option>';
                            }
                            ?>
                        </select>
                        <label for="gander_id">Gander</label>
                        <select name="gander_id">
                            <?php
                            foreach ($gander as $gander) {
                                $selected = '';
                                if ($gander['id'] == $mahasiswa['gander_id']) $selected = 'selected';
                                echo '<option value="' . $gander['id'] . '" ' . $selected . '>' . $gander['gander'] . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">SIMPAN</button>
                    <a href="Halaman_Utama.php" class="btn btn-warning">Kembali</a>
            </div>
        </div>
    </div>
</body>

</html>