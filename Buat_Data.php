<?php
include 'Koneksi.php';
include 'Prodi.php';
include 'Gander.php';
mysqli_close($link);
if (count($prodi) == 0) {
    echo '<p>Data prodi belum tersedia</p>';
    echo '<a href="Halaman_Utama.php"><button type="submit" class="btn btn-warning">Halaman Utama</button></a>';
    exit();
}
if (count($gander) == 0) {
    echo '<p>Data gander belum tersedia</p>';
    echo '<a href="Halaman_Utama.php"><button type="submit" class="btn btn-warning">Halaman Utama</button></a>';
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Buat Data Baru</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        .mx-auto {
            width: 1000px;
        }

        .card {
            margin-top: 10px;
        }
    </style>
</head>

<body>
    <div class="mx-auto">
        <div class="card">
            <div class="card-header text-white bg-secondary">
                BUAT DATA BARU
            </div>
            <div class="card-body">
                <form action="Simpan.php" method="post">
                    <div class="mb-3 row">
                        <label for="nim">NIM</label>
                        <input type="text" name="nim" />
                        <label for="nama_mahasiswa">Nama</label>
                        <input type="text" name="nama_mahasiswa" />
                        <label for="prodi_id">Prodi</label>
                        <select name="prodi_id">
                            <option value="">-Pilih-</option>
                            <?php
                            foreach ($prodi as $prodi) {
                                echo '<option
        value="' . $prodi['id'] . '">' . $prodi['prodi'] . '</option>';
                            }
                            ?>
                        </select>
                        <label for="gander_id">Gander</label>
                        <select name="gander_id">
                            <option value="">-Pilih-</option>
                            <?php
                            foreach ($gander as $gander) {
                                echo '<option
        value="' . $gander['id'] . '">' . $gander['gander'] . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">SIMPAN</button>
                    <a href="Halaman_Utama.php" class="btn btn-warning">Kembali</a>
            </div>
        </div>
    </div>
</body>

</html>