<?php
include 'Koneksi.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Mahasiswa</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        .mx-auto {
            width: 1000px;
        }

        .card {
            margin-top: 10px;
        }
    </style>
</head>

<body>
    <div class="mx-auto">
        <div class="card">
            <div class="card-header text-white bg-secondary">
                DATA MAHASISWA
            </div>
            <div class="card-body">
                <form action="Buat_Data.php">
                    <button type="submit" class="btn btn-primary">BUAT DATA BARU</button>
                </form>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">&nbsp;</th>
                            <th scope="col">NIM</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Prodi</th>
                            <th scope="col">Gander</th>
                        </tr>
                    </thead>
                    <?php
                    $offset = 0;
                    $rowperpage = 10;
                    $banyakrow = 0;
                    if (isset($_GET['o'])) $offset = $_GET['o'];
                    $result = mysqli_query($link, "SELECT m.nim,m.nama_mahasiswa,p.prodi,g.gander
FROM mahasiswa m
INNER JOIN prodi p ON p.id=m.prodi_id 
INNER JOIN gander g ON m.gander_id=g.id
ORDER BY m.nim
LIMIT $offset,$rowperpage");
                    if ($result) {
                        while ($row = mysqli_fetch_row($result)) {
                            $banyakrow++;
                            echo
                            '<tr>
<td>
<a href="Rubah_Data.php?nim=' . $row[0] . '"><button type="submit" class="btn btn-info">EDIT</button></a>&nbsp;&nbsp;
<a href="Delete.php?nim=' . $row[0] . '" onclick="return confirm(\'Hapus?\')"><button type="submit" class="btn btn-danger">HAPUS</button></a>
</td>
<td>' . $row[0] . '</td><td>' . $row[1] . '</td><td>' . $row[2] . '</td><td>' . $row[3] . '</td></tr>';
                        }
                        mysqli_free_result($result);
                    }
                    mysqli_close($link);
                    ?>
                    </tbody>
                </table>
                <?php
                $prev_offset = $offset - $rowperpage;
                $next_offset = $offset + $rowperpage;
                if ($offset > 0) echo '<a
href="Halaman_Utama.php?o=' . $prev_offset . '">&nbsp;&lt;&lt;&nbsp;</a>';
                if ($banyakrow == $rowperpage) echo '<a
href="Halaman_Utama.php?o=' . $next_offset . '">&nbsp;&gt;&gt;&nbsp;</a>';
                ?>
            </div>
        </div>
    </div>
</body>

</html>