<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Simpan</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <style>
    .mx-auto {
      width: 1000px;
    }

    .card {
      margin-top: 10px;
    }
  </style>
</head>

<body>
  <div class="mx-auto">
    <?php
    include 'Koneksi.php';
    if (
      $_POST['nim'] == '' || $_POST['nama_mahasiswa'] == '' ||
      !isset($_POST['prodi_id']) || !isset($_POST['gander_id'])
    ) {
      echo '<p>Data mahasiswa tidak lengkap</p>';
      echo '<a href="Halaman_Utama.php" "><button type="submit" class="btn btn-warning">Halaman Utama</button></a>';
      mysqli_close($link);
      exit();
    }
    $nim            = $_POST['nim'];
    $nama_mahasiswa = $_POST['nama_mahasiswa'];
    $prodi_id       = $_POST['prodi_id'];
    $gander_id      = $_POST['gander_id'];
    $q = mysqli_query($link, "INSERT INTO mahasiswa(nim,nama_mahasiswa,prodi_id,gander_id) VALUES ('$nim','$nama_mahasiswa','$prodi_id','$gander_id')");
    if ($q) {
      echo '<p>' . mysqli_affected_rows($link) . ' Data telah disimpan.</p>';
    } else {
      echo '<p>Error: ' . mysqli_error($link) . '</p>';
    }
    mysqli_close($link);
    echo '<a href="Halaman_Utama.php" "><button type="submit" class="btn btn-warning">Halaman Utama</button></a>';
    ?>
  </div>
</body>

</html>